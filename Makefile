######################################################################
#  Project
######################################################################

BINARY		= main
LIBFILES 	:= $(wildcard ../libs/*.c) $(wildcard ../libs/*/*.c)
#Remove duplicate files
LIBFILES    := $(filter-out ../libs/delay.c,$(LIBFILES))
LIBFILES    := $(filter-out ../libs/clock.c,$(LIBFILES))
LIBFILES    := $(filter-out ../libs/timer.c,$(LIBFILES))
LIBFILES    := $(filter-out ../libs/uart.c,$(LIBFILES))
LIBFILES    := $(filter-out ../libs/ring.c,$(LIBFILES))
LIBFILES    := $(filter-out ../libs/flash.c,$(LIBFILES))
LIBFILES    := $(filter-out ../libs/rtc.c,$(LIBFILES))
SRCFILES	= $(wildcard ./lib/*.c) $(wildcard .*/*.c) $(LIBFILES)



all: elf bin

include ../Makefile.incl

# End
