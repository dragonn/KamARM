#ifndef ENCODERS_H
#define ENCODERS_H

#include <libopencm3/stm32/gpio.h>

void encoders_init(void);

struct ENCODER {
   uint32_t tim;

   uint16_t apin;
   uint32_t aport;
   uint8_t  aaf;

   uint16_t bpin;
   uint32_t bport;
   uint8_t  baf;

   uint16_t datapin;
   uint32_t dataport;

   bool dataarray[13];
   uint16_t data;
};

extern volatile struct ENCODER encoders[];

#define _ENCCLK_PORT GPIOD
#define _ENCCLK_PIN GPIO9
#define _ENCCLK_TOGGLE gpio_toggle(_ENCCLK_PORT, _ENCCLK_PIN)
#define _ENCCLK_SET gpio_set(_ENCCLK_PORT, _ENCCLK_PIN)
#endif