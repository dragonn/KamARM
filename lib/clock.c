#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/nvic.h>

#include "clock.h"

//uruchamienie głównych zegarów
void clock_init(void) {
    //zegar główny na 72MHz z kwarcu zewnętrznego
    //rcc_backupdomain_reset();
    //rcc_clock_setup_in_hsi_out_48mhz();
    //rcc_clock_setup_in_hse_8mhz_out_72mhz();
    rcc_clock_setup_hse_3v3(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

    //taktowanie portów GPIO
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_GPIOD);
    rcc_periph_clock_enable(RCC_GPIOE);
    //taktowanie USART
    rcc_periph_clock_enable(RCC_USART3);

    //taktowanie pwm
    rcc_periph_clock_enable(RCC_TIM1);
    rcc_periph_clock_enable(RCC_TIM2);
    rcc_periph_clock_enable(RCC_TIM9);
    rcc_periph_clock_enable(RCC_TIM12);
    
    //taktowanie enkoderów
    rcc_periph_clock_enable(RCC_TIM5);
    rcc_periph_clock_enable(RCC_TIM3);
    rcc_periph_clock_enable(RCC_TIM8);
    rcc_periph_clock_enable(RCC_TIM4);

    //taktowanie SSI
    rcc_periph_clock_enable(RCC_TIM7);
}

//uruchamienie głównega przerwania systemowego
void clock_systick(unsigned int  us) {
    unsigned int  ticks = us*168;
    if (ticks > 0xFFFFFF) {
        systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
        systick_set_reload(ticks/8);
    } else {
        systick_set_clocksource(STK_CSR_CLKSOURCE_AHB);
        systick_set_reload(ticks);
    }
    systick_interrupt_enable();
    systick_counter_enable();
    nvic_set_priority(NVIC_SYSTICK_IRQ, 0xFF);
}