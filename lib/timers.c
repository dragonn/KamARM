#include <libopencm3/stm32/timer.h>

#include "timers.h"
#include "pwm.h"

void timer1_init(void);
void timer9_init(void);
void timer2_init(void);

void timers_init(void) {
    timer1_init();
    timer9_init();
    timer2_init();
}

void timer1_init(void) {
    //timer_reset(TIM1);
    timer_set_mode(TIM1, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_CENTER_1, TIM_CR1_DIR_UP);
    timer_set_prescaler(TIM1, 83);
    timer_set_period(TIM1, 1000000/PWM_FREQ);
    timer_enable_break_main_output(TIM1);

    timer_enable_oc_output(TIM1, TIM_OC1);
    timer_set_oc_mode(TIM1, TIM_OC1, TIM_OCM_PWM1);

    timer_enable_oc_output(TIM1, TIM_OC2);
    timer_set_oc_mode(TIM1, TIM_OC2, TIM_OCM_PWM1);

    timer_enable_oc_output(TIM1, TIM_OC3);
    timer_set_oc_mode(TIM1, TIM_OC3, TIM_OCM_PWM1);

    timer_enable_oc_output(TIM1, TIM_OC4);
    timer_set_oc_mode(TIM1, TIM_OC4, TIM_OCM_PWM1);
    timer_enable_counter(TIM1);
}


void timer9_init(void) {
    //timer_reset(TIM1);
    timer_set_mode(TIM9, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_CENTER_1, TIM_CR1_DIR_UP);
    timer_set_prescaler(TIM9, 167);
    timer_set_period(TIM9, 1000000/PWM_FREQ);
    //timer_enable_break_main_output(TIM9);

    timer_enable_oc_output(TIM9, TIM_OC1);
    timer_set_oc_mode(TIM9, TIM_OC1, TIM_OCM_PWM1);

    timer_enable_oc_output(TIM9, TIM_OC2);
    timer_set_oc_mode(TIM9, TIM_OC2, TIM_OCM_PWM1);
    timer_enable_counter(TIM9);
}


void timer2_init(void) {
    //timer_reset(TIM1);
    timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_CENTER_1, TIM_CR1_DIR_UP);
    timer_set_prescaler(TIM2, 41);
    timer_set_period(TIM2, 1000000/PWM_FREQ);
    //timer_enable_break_main_output(TIM9);

    timer_enable_oc_output(TIM2, TIM_OC1);
    timer_set_oc_mode(TIM2, TIM_OC1, TIM_OCM_PWM1);

    timer_enable_oc_output(TIM2, TIM_OC3);
    timer_set_oc_mode(TIM2, TIM_OC3, TIM_OCM_PWM1);

    timer_enable_oc_output(TIM2, TIM_OC4);
    timer_set_oc_mode(TIM2, TIM_OC4, TIM_OCM_PWM1);

    timer_enable_counter(TIM2);
}