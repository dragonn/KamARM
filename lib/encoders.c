#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include "encoders.h"

volatile struct ENCODER encoders[6];
volatile struct ENCODER *enc1;
void encoders_init(void) {
    //ENCO_CLK
    gpio_mode_setup(_ENCCLK_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, _ENCCLK_PIN);
    gpio_set(_ENCCLK_PORT, _ENCCLK_PIN);
    //ENC1
    encoders[0].aport=GPIOA;
    encoders[0].apin=GPIO0;
    encoders[0].aaf=GPIO_AF2;

    encoders[0].bport=GPIOA;
    encoders[0].bpin=GPIO1;
    encoders[0].baf=GPIO_AF2;

    encoders[0].dataport=GPIOD;
    encoders[0].datapin=GPIO0;
    encoders[0].tim=TIM5;
    enc1=&encoders[0];
    //ENC2
    encoders[1].aport=GPIOA;
    encoders[1].apin=GPIO6;
    encoders[1].aaf=GPIO_AF2;

    encoders[1].bport=GPIOA;
    encoders[1].bpin=GPIO7;
    encoders[1].baf=GPIO_AF2;

    encoders[1].dataport=GPIOD;
    encoders[1].datapin=GPIO1;
    encoders[1].tim=TIM3;

    //ENC3
    encoders[2].aport=GPIOC;
    encoders[2].apin=GPIO6;
    encoders[2].aaf=GPIO_AF3;

    encoders[2].bport=GPIOC;
    encoders[2].bpin=GPIO7;
    encoders[2].baf=GPIO_AF3;

    encoders[2].dataport=GPIOD;
    encoders[2].datapin=GPIO2;
    encoders[2].tim=TIM8;

    //ENC4
    encoders[3].aport=GPIOD;
    encoders[3].apin=GPIO12;
    encoders[3].aaf=GPIO_AF2;

    encoders[3].bport=GPIOD;
    encoders[3].bpin=GPIO13;
    encoders[3].baf=GPIO_AF2;

    encoders[3].dataport=GPIOD;
    encoders[3].datapin=GPIO3;
    encoders[3].tim=TIM4;

    //ENC5
    encoders[4].aport=GPIOD;
    encoders[4].apin=GPIO14;
    encoders[4].aaf=0;

    encoders[4].bport=GPIOD;
    encoders[4].bpin=GPIO15;
    encoders[4].baf=0;

    encoders[4].dataport=GPIOD;
    encoders[4].datapin=GPIO4;
    encoders[4].tim=0;

    //ENC6
    encoders[5].aport=0;
    encoders[5].apin=0;
    encoders[5].aaf=0;

    encoders[5].bport=0;
    encoders[5].bpin=0;
    encoders[5].baf=0;

    encoders[5].dataport=GPIOD;
    encoders[5].datapin=GPIO5;
    encoders[5].tim=0;


    for (uint8_t i=0; i<4; i++) {
        encoder_conf(&encoders[i]);
    }

    /*timer_set_period(TIM5, 8192);
	timer_slave_set_mode(TIM5, 0x3); // encoder
	timer_ic_set_input(TIM5, TIM_IC1, TIM_IC_IN_TI1);
	timer_ic_set_input(TIM5, TIM_IC2, TIM_IC_IN_TI2);
	timer_enable_counter(TIM5);*/
}

void encoder_conf(volatile struct ENCODER *encoder) {
    //A
    gpio_mode_setup(encoder->aport, GPIO_MODE_AF, GPIO_PUPD_NONE, encoder->apin);
    gpio_set_af(encoder->aport, encoder->aaf, encoder->apin);

    //B
    gpio_mode_setup(encoder->bport, GPIO_MODE_AF, GPIO_PUPD_NONE, encoder->bpin);
    gpio_set_af(encoder->bport, encoder->baf, encoder->bpin);

    //DATA
    gpio_mode_setup(encoder->dataport, GPIO_MODE_INPUT, GPIO_PUPD_NONE, encoder->datapin);

    
    timer_set_period(encoder->tim, 8192);
	timer_slave_set_mode(encoder->tim, TIM_SMCR_SMS_EM3); // encoder
	timer_ic_set_input(encoder->tim, TIM_IC1, TIM_IC_IN_TI1);
	timer_ic_set_input(encoder->tim, TIM_IC2, TIM_IC_IN_TI2);
	timer_enable_counter(encoder->tim);

}