#include "pid.h"
#include "pwm.h"
#include "dir.h"
#include "flash.h"
#include "crc.h"

#include <libopencm3/stm32/timer.h>

volatile struct PID pid1;
volatile struct PID pid2;
volatile struct PID pid3;
volatile struct PID pid4;
int abs(int v);

int abs(int v) {
    return v * ( (v<0) * (-1) + (v>0));
}

void pid_init(void) {
    /*pid1.deviation1=0;
    pid1.deviation2=0;
    pid1.deviation3=0;
    pid1.control1=0;
    pid1.control2=0;


    pid1.dead=0;
    pid1.prop=0.416;
    pid1.iner=16.68;

    pid1.pwm=&pwm1;
    pid1.dir=&dir1;
    pid1.desired_value=4025;
    pid1.current=&TIM_CNT(TIM1);
    pid1.center=3414;
    pid1.enabled=false;

    pid2.deviation1=0;
    pid2.deviation2=0;
    pid2.deviation3=0;
    pid2.control1=0;
    pid2.control2=0;

    pid2.dead=0;
    pid2.prop=0.416;
    pid2.iner=16.68;

    pid2.pwm=&pwm2;
    pid2.dir=&dir2;
    pid2.desired_value=4025;
    pid2.current=&TIM_CNT(TIM4);
    pid2.center=5687;
    pid2.enabled=false;

    pid3.deviation1=0;
    pid3.deviation2=0;
    pid3.deviation3=0;
    pid3.control1=0;
    pid3.control2=0;

    pid3.dead=0;
    pid3.prop=0.416;
    pid3.iner=16.68;

    pid3.pwm=&pwm3;
    pid3.dir=&dir3;
    pid3.desired_value=4025;
    pid3.current=&TIM_CNT(TIM3);
    pid3.center=3867;
    pid3.enabled=false;


    pid4.deviation1=0;
    pid4.deviation2=0;
    pid4.deviation3=0;
    pid4.control1=0;
    pid4.control2=0;

    pid4.dead=0;
    pid4.prop=0.416;
    pid4.iner=16.68;

    pid4.pwm=&pwm4;
    pid4.dir=&dir4;
    pid4.desired_value=4025;
    pid4.current=&TIM_CNT(TIM2);
    pid4.center=4323;
    pid4.enabled=false;*/
}

void pid_calc(volatile struct PID *pid) {
    /*if (pid->enabled) {
        pid->deviation1=pid->desired_value - *pid->current;
        
        if (abs(pid->deviation1) > 80) {
            pid->sum_p=(pid->deviation1 - pid->deviation2) * pid->prop;
            pid->sum_i=pid->deviation1 * pid->iner;
            pid->sum_d=(pid->deviation1 - (2 * pid->deviation2) + pid->deviation3) + pid->dead;

            pid->control1=pid->sum_p + pid->sum_i + pid->sum_d + pid->control2;
            pid->deviation3=pid->deviation2;
            pid->deviation2=pid->deviation1;

            if (pid->control1 > PWM_MAX) pid->control1 = PWM_MAX;
            if (pid->control1 < -PWM_MAX) pid->control1 = -PWM_MAX;

            pid->control2=pid->control1;

            if (pid->control1 > 0) {
                pid->pwm->set = pid->control1;
                dir_backward(pid->dir);
            } if (pid->control1 < 0) {
                pid->pwm->set = -pid->control1;
                dir_forward(pid->dir);
            } else if (pid->control1 == 0) {
                pid->pwm->set=PWM_MIN;
            }
        } else {
            pid->deviation1=0;
            pid->deviation2=0;
            pid->deviation3=0;
            pid->control2=0;
            pid->pwm->set=0;
        }
    } else {
        pid->deviation1=0;
        pid->deviation2=0;
        pid->deviation3=0;
        pid->control2=0;
        pid->pwm->set=0;
        dir_clear(pid->dir);
    }*/
}

void pid_angle(int16_t angle_set1, int16_t angle_set2) {
    angle_set1 = ((angle_set1 - 90) * 22.75);
    angle_set2 = ((angle_set2 - 90) * 22.75);

    pid1.desired_value = angle_set1 + pid1.center;
    pid3.desired_value = angle_set1 + pid3.center;
    pid2.desired_value = angle_set2 + pid2.center;
    pid4.desired_value = angle_set2 + pid4.center;
    pid1.enabled = true;
    pid2.enabled = true;
    pid3.enabled = true;
    pid4.enabled = true;

}

void pid_save(void) {
    uint8_t block[16];
    block[0]=((pid1.center >> 8) & 0xff);
	block[1]=((pid1.center >> 0) & 0xff);

    block[2]=((pid2.center >> 8) & 0xff);
	block[3]=((pid2.center >> 0) & 0xff);

    block[4]=((pid3.center >> 8) & 0xff);
	block[5]=((pid3.center >> 0) & 0xff);

    block[6]=((pid4.center >> 8) & 0xff);
	block[7]=((pid4.center >> 0) & 0xff);

    uint16_t crc = crc16_calc(&block, 8);

    block[8]=((crc >> 8) & 0xff);
	block[9]=((crc >> 0) & 0xff);

    flash_update(0x801F000, &block, 16);
}

void pid_read(void) {
    uint8_t block[16];
    flash_read(0x801F000, 16, &block);
    uint16_t crc_check = crc16_calc(&block, 8);
    uint16_t crc_org = (block[8] << 8) + block[9];
    if (crc_org == crc_check) {
        pid1.center = (block[0] << 8) + block[1];
        pid2.center = (block[2] << 8) + block[3];
        pid3.center = (block[4] << 8) + block[5];
        pid4.center = (block[6] << 8) + block[7];
    }
}