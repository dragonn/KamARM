#include <libopencm3/stm32/usart.h>
#include "ring.h"
//#include "timer.h"

//buffor wyjściowy
struct ring output1_ring;
uint8_t output1_ring_buffer[BUFFER_SIZE];


//buffor wejściowy
struct ring input1_ring;
uint8_t input1_ring_buffer[BUFFER_SIZE];



int _write(int file, char *ptr, int len);

//czyszczenie i inicjalzacja buffora
static void ring_clear(struct ring *ring, uint8_t *buf, ring_size_t size)
{
	ring->data = buf;
	ring->size = size;
	ring->begin = 0;
	ring->end = 0;
}

//wpisywanie znaku do buffora
int32_t ring_write_ch(struct ring *ring, char ch)
{
	if (((ring->end + 1) % ring->size) != ring->begin) {
		ring->data[ring->end++] = ch;
		ring->end %= ring->size;
		return (uint32_t)ch;
	}

	return -1;
}

//wpisywanie danych do buffora
int32_t ring_write(struct ring *ring, char *data, ring_size_t size)
{
	int32_t i;

	for (i = 0; i < size; i++) {
		if (ring_write_ch(ring, data[i]) < 0)
			return -i;
	}

	return i;
}

//czytanie znaków z buffora
int32_t ring_read_ch(struct ring *ring, char *ch)
{
	int32_t ret = -1;

	if (ring->begin != ring->end) {
		ret = ring->data[ring->begin++];
		ring->begin %= ring->size;
		if (ch)
			*ch = ret;
	}

	return ret;
}



/*int32_t ring_read(struct ring *ring, char *data, ring_size_t size, uint16_t timeout)
{
	int32_t i=0;
	int64_t now = timer_milis;
	while (i < size && timer_milis-now < timeout) {
		if (ring_read_ch(ring, data + i) != -1) {
			i++;
		}
	}
	return -i;
}*/


//inicjalizacja buffora dla pierwszeo UART
void ring1_init(void) {
    ring_clear(&output1_ring, output1_ring_buffer, BUFFER_SIZE);
    ring_clear(&input1_ring, input1_ring_buffer, BUFFER_SIZE);
}