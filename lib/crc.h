#ifndef CRC_H
#define CRC_H

#include <stdio.h>

uint16_t crc16_calc(uint8_t *str, uint8_t len);

#endif