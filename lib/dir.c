#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>

#include "dir.h"

volatile struct DIR dirs[10];

void dir_conf(volatile struct DIR *dir);

void dir_init(void) {
    dirs[0].portsA=GPIOF;
    dirs[0].pinA=GPIO14;
    dirs[0].portsB=GPIOF;
    dirs[0].pinB=GPIO15;
    
    dirs[1].portsA=GPIOG;
    dirs[1].pinA=GPIO0;
    dirs[1].portsB=GPIOG;
    dirs[1].pinB=GPIO1;

    dirs[2].portsA=GPIOG;
    dirs[2].pinA=GPIO2;
    dirs[2].portsB=GPIOG;
    dirs[2].pinB=GPIO3;

    dirs[3].portsA=GPIOG;
    dirs[3].pinA=GPIO4;
    dirs[3].portsB=GPIOG;
    dirs[3].pinB=GPIO5;

    dirs[4].portsA=GPIOG;
    dirs[4].pinA=GPIO6;
    dirs[4].portsB=GPIOG;
    dirs[4].pinB=GPIO7;

    dirs[5].portsA=GPIOG;
    dirs[5].pinA=GPIO8;
    dirs[5].portsB=GPIOG;
    dirs[5].pinB=GPIO9;

    dirs[6].portsA=GPIOG;
    dirs[6].pinA=GPIO10;
    dirs[6].portsB=GPIOG;
    dirs[6].pinB=GPIO11;

    dirs[7].portsA=GPIOG;
    dirs[7].pinA=GPIO12;
    dirs[7].portsB=GPIOG;
    dirs[7].pinB=GPIO13;

    dirs[8].portsA=GPIOG;
    dirs[8].pinA=GPIO12;
    dirs[8].portsB=GPIOG;
    dirs[8].pinB=GPIO13;

    for (uint8_t i=0; i<9; i++) {
        dir_conf(&dirs[i]);
    }
}

void dir_conf(volatile struct DIR *dir) {
    gpio_mode_setup(dir->portsA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, dir->pinA);
    gpio_mode_setup(dir->portsB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, dir->pinB);
}

void dir_forward(volatile struct DIR *dir) {
    gpio_set(dir->portsA, dir->pinA);
    gpio_clear(dir->portsB, dir->pinB);
}

void dir_backward(volatile struct DIR *dir) {
    gpio_clear(dir->portsA, dir->pinA);
    gpio_set(dir->portsB, dir->pinB);

}

void dir_clear(volatile struct DIR *dir) {
    gpio_clear(dir->portsA, dir->pinA);
    gpio_clear(dir->portsB, dir->pinB);
}