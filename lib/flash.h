#ifndef FLASH_H
#define FLASH_H


#define FLASH_PAGE_NUM_MAX 127
#define FLASH_PAGE_SIZE 0x400
#define FLASH_WRONG_DATA_WRITTEN 0x80
#define FLASH_BASE 0

void flash_read(uint32_t start_address, uint16_t num_elements, uint8_t *output_data);
//uint32_t flash_write(uint32_t start_address, uint8_t *input_data, uint16_t num_elements);
uint32_t flash_update(uint32_t start_address, uint8_t *input_data, uint16_t num_elements);
#endif