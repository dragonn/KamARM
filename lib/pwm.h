#ifndef PWM_H
#define PWM_H

#include <libopencm3/stm32/gpio.h>

void pwm_init(void);
struct PWM {
   volatile uint32_t *set;
   uint16_t pin;
   uint32_t ports;
   uint8_t af;
};

extern volatile struct PWM pwms[];
#define PWM_FREQ 500

#endif