
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/rcc.h>
#include "ssi.h"
#include "encoders.h"

#define DELAY 1
void delay(uint32_t ticks);
void sort(uint16_t arr[], uint8_t count);

volatile uint8_t tim7_count = 0;

int bitstoins(bool arr[], int count);

void ssi_init(void) {
	rcc_periph_reset_pulse(RST_TIM7);
	nvic_enable_irq(NVIC_TIM7_IRQ);
	timer_set_mode(TIM7, TIM_CR1_CKD_CK_INT,
		       TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

	//timer_set_prescaler(TIM7, ((rcc_apb1_frequency * 2) / 5000));
	timer_set_prescaler(TIM7, ((rcc_apb1_frequency * 2) / 2000000));
	timer_disable_preload(TIM7);
	timer_continuous_mode(TIM7);
	timer_set_period(TIM7, 2);
	timer_update_on_overflow(TIM7);
	/* Set the capture compare value for OC1. */
	//timer_set_oc_value(TIM7, TIM_OC1, 500);
	timer_enable_counter(TIM7);
	timer_enable_irq(TIM7, TIM_DIER_UIE);
}


void tim7_isr(void) {
	if (timer_get_flag(TIM7, TIM_SR_UIF)) {
		/* Clear compare interrupt flag. */
		if (tim7_count < 2) {
			_ENCCLK_TOGGLE;
			tim7_count++;
		} else if (tim7_count < 29) {
			if ((tim7_count + 1) % 2 == 0) {
				for (uint8_t i = 0; i< 6; i++) {
					encoders[i].dataarray[(tim7_count-2)/2]=gpio_get(encoders[i].dataport, encoders[i].datapin) ? true : false;
				}
			}
			_ENCCLK_TOGGLE;
			tim7_count++;
		} else {
			_ENCCLK_SET;
			for (uint8_t i = 0; i< 6; i++) {
				encoders[i].data = bitstoins(encoders[i].dataarray, 13);
			}
			timer_set_period(TIM7, 65535);
			if (tim7_count > 31) {
				tim7_count=0;
				timer_set_period(TIM7, 2);
			} else {
				tim7_count++;
			}
		}
		
		timer_clear_flag(TIM7, TIM_SR_UIF); 
	}
}

void delay(uint32_t ticks) {
	for (uint32_t i=0; i < ticks; i++) {
		__asm__("NOP");
	}
}

void sort(uint16_t arr[], uint8_t count) {
	for (int i = 0; i < count; i++)	{
		for (int j = 0; j < count; j++) {
			if (arr[j] > arr[i]) {
				int tmp = arr[i];         //Using temporary variable for storing last value
				arr[i] = arr[j];            //replacing value
				arr[j] = tmp;             //storing last value
			}  
		}
	}
}

//#define DELAYASM __asm__("NOP");

#define DELAYASM delay(8);
int bitstoins(bool arr[], int count)
{
    int ret = 0;
    int tmp;
    for (int i = 0; i < count; i++) {
        tmp = arr[i];
        ret |= tmp << (count - i - 1);
    }
    return ret;
}

void ssi_read(uint16_t *enc1_out, uint16_t *enc2_out, uint16_t *enc3_out, uint16_t *enc4_out)  {
	uint16_t enc1_means[10], enc2_means[10], enc3_means[10], enc4_means[10];
	for (int c=0; c < 10; c++) {
		gpio_toggle(GPIOC, GPIO14);
		DELAYASM;
		gpio_toggle(GPIOC, GPIO14);
		//delay(DELAY);
		DELAYASM;
		//uint32_t enc1_data=0, enc2_data=0, enc3_data=0, enc4_data=0;

		bool enc1_array[13];
		bool enc2_array[13];
		bool enc3_array[13];
		bool enc4_array[13];

		//enc1_array[13]=255;
		//enc1_data=(enc1_data<<1) +1;
		for (int i=0; i<27; i++) {
			if ((i + 1) % 2 == 0) {
				DELAYASM;
				//gpio_toggle(GPIOC, GPIO13);	
				/*enc1_data=enc1_data | ENC1_SSI_DATA ? 1 : 0;
				enc1_data=enc1_data << 1;

				enc2_data=enc2_data | ENC2_SSI_DATA;
				enc2_data=enc2_data << 1;

				enc3_data=enc3_data | ENC3_SSI_DATA;
				enc3_data=enc3_data << 1;

				enc4_data=enc4_data | ENC4_SSI_DATA;
				enc4_data=enc4_data << 1; */
				/*enc1_array[i/2]=ENC1_SSI_DATA ? true : false;
				enc2_array[i/2]=ENC2_SSI_DATA ? true : false;
				enc3_array[i/2]=ENC3_SSI_DATA ? true : false;
				enc4_array[i/2]=ENC4_SSI_DATA ? true : false;*/
				
			} else {
				DELAYASM;
			}
			//__asm__("NOP");
			gpio_toggle(GPIOC, GPIO14);
			//delay(1);
			//__asm__("NOP");//
		}
		gpio_set(GPIOC, GPIO14);
		

		uint16_t enc1_data = bitstoins(enc1_array, 13);
		uint16_t enc2_data = bitstoins(enc2_array, 13);
		uint16_t enc3_data = bitstoins(enc3_array, 13);
		uint16_t enc4_data = bitstoins(enc4_array, 13);

		enc1_means[c]=enc1_data;
		enc2_means[c]=enc2_data;
		enc3_means[c]=enc3_data;
		enc4_means[c]=enc4_data;

		delay(8000);
	}

	sort(enc1_means, 10);
	sort(enc2_means, 10);
	sort(enc3_means, 10);
	sort(enc4_means, 10);

	uint16_t enc1_mean = (enc1_means[4] + enc1_means[5])/2;
	uint16_t enc2_mean = (enc2_means[4] + enc2_means[5])/2;
	uint16_t enc3_mean = (enc3_means[4] + enc3_means[5])/2;
	uint16_t enc4_mean = (enc4_means[4] + enc4_means[5])/2;

	*enc1_out=enc1_mean;
	*enc2_out=enc2_mean;
	*enc3_out=enc3_mean;
	*enc4_out=enc4_mean;
}