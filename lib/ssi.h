#ifndef SSI_H
#define SSI_H

void ssi_init(void);
void ssi_read(uint16_t *enc1_out, uint16_t *enc2_out, uint16_t *enc3_out, uint16_t *enc4_out);

#endif