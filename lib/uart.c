#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/nvic.h>

#include <stdio.h>

#include "uart.h"
#include "ring.h"
#include "cobs.h"
#include "crc.h"
#include "pid.h"
#include "pwm.h"
#include "dir.h"
//#include "rtc.h"

struct BUFFER uart1_out_buffer;
volatile uint32_t uart1_last_message;
volatile uint32_t uart1_send_message;
void uart1_command(uint8_t *buffer, uint8_t length) {
	length-=1;
	//uint8_t out[64];
	cobs_decode(buffer, length, buffer);
	if (buffer[0] == 0x01) {		
		uint16_t crc_check = crc16_calc(buffer, length-3);
		uint16_t crc_org = (buffer[length-3] << 8) + buffer[length-2];
		if (crc_check == crc_org) {
			uint8_t cmd = buffer[2];
			uint8_t sender = buffer[1];
			switch (cmd) {
				//sterowanie jazdą
				case 0: {
					//uart1_last_message=rtc_get();
					uint8_t angle1 = buffer[3];
					uint8_t angle2 = buffer[4];
					uint8_t driving_pwm = buffer[5];
					uint8_t driving_direction = buffer[6];

					pid_angle(angle1, angle2);
					//pwm5.set=driving_pwm;

					/*switch (driving_direction) {
						case 0: {
							dir_forward(&dir5);
							break;
						}

						case 1: {
							dir_backward(&dir5);
							break;
						}

						case 3: {
							dir_clear(&dir5);
							break;
						}

						default: {
							dir_clear(&dir5);
							break;
						}
					}*/

					pid1.enabled=true;
					pid2.enabled=true;
					pid3.enabled=true;
					pid4.enabled=true;
				break; }

				//kalibracja
				case 1: {
					uint8_t motor = buffer[3] / 10;
					int8_t direction = buffer[3] % 10;
					uint16_t angle_fix = buffer[4]  * 22.75;
					if (direction == 1) {
						direction = -1;
					} else {
						direction = 1;
					}
 
					switch(motor) {
						case 1: {
							pid1.center += angle_fix * direction;
							break;
						}

						case 2: {
							pid2.center += angle_fix * direction;
							break;
						}

						case 3: {
							pid3.center += angle_fix * direction;
							break;
						}

						case 4: {
							pid4.center += angle_fix * direction;
							break;
						}
					}

					pid_save();
				}
			}

			uint8_t answer[20];
			uint8_t cobs[20];
			answer[0]=sender;
			answer[1]=0x01;
			answer[2]=0x5F;
			answer[3]=0xF0;

			uint16_t crc_answer = crc16_calc(&answer, 4);
			answer[4]=((crc_answer >> 8) & 0xff);
			answer[5]=((crc_answer >> 0) & 0xff);
			cobs_encode(&answer, 6, &cobs);
			cobs[7]=0x00;
			usart1_write(&cobs,8);
		}
	}
}

//uruchamianie pierwszego uarta
void uart1_init(void) {
    //inicjalizacja bufforów
	uart1_last_message=0;
    ring1_init();
    nvic_enable_irq(NVIC_USART3_IRQ);

    //priorytet przerwania
    nvic_set_priority(NVIC_USART3_IRQ, 0);
    //inicjalizacja portów
    //gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_USART3_TX);
    //gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_PULLDOWN, GPIO_USART3_RX);
	//r/s dla 485
	gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO1);
	gpio_clear(GPIOB, GPIO1);
    //parametry portu
    usart_set_baudrate(USART3, 115200);
    usart_set_databits(USART3, 8);
	usart_set_stopbits(USART3, USART_STOPBITS_1);
	usart_set_mode(USART3, USART_MODE_TX_RX);
	usart_set_parity(USART3, USART_PARITY_NONE);
	usart_set_flow_control(USART3, USART_FLOWCONTROL_NONE);

    //przerwanie odbierania znaków
    USART_CR1(USART3) |= USART_CR1_RXNEIE;

    //włączenie uart
    usart_enable(USART3);
}


void usart3_isr(void) {
	//sprawdź czy przerwanie przyszło z odbierania znaków
	if (((USART_CR1(USART3) & USART_CR1_RXNEIE) != 0) &&
	    ((USART_SR(USART3) & USART_SR_RXNE) != 0)) {
		char recv=usart_recv(USART3);
        //odczytaj dane z portu i zapisz do buffora
		if (uart1_out_buffer.position == 63) {
			uart1_out_buffer.position=0;
		}

		if (recv != '\0') {
			uart1_out_buffer.data[uart1_out_buffer.position++]=recv;
		} else {
			uart1_out_buffer.data[uart1_out_buffer.position++]=recv;
			uart1_command(&uart1_out_buffer.data, uart1_out_buffer.position);
			uart1_out_buffer.position=0;
		}
	}

	//sprawdzamy czy przerwanie przyszło z wysłania znaków
	if (((USART_CR1(USART3) & USART_CR1_TXEIE) != 0) &&
	    ((USART_SR(USART3) & USART_SR_TXE) != 0)) {

		int32_t data;

		data = ring_read_ch(&output1_ring, NULL);

		if (data == -1) {
			//koniec wysłania danych, kasujemy przewanie
			if ((USART_SR(USART3) & USART_SR_TC) != 0) {
				USART_CR1(USART3) &= ~USART_CR1_TXEIE;
				gpio_clear(GPIOB, GPIO1);
			}
			
		} else {
			//wysłamy dane
			gpio_set(GPIOB, GPIO1);
			usart_send(USART3, data);
		}
	}

	/*if (((USART_CR1(USART3) & USART_CR1_TCIE) != 0) && 
	   ((USART_SR(USART3) & USART_SR_TXE) != 0)) {
		gpio_clear(GPIOB, GPIO1);
		USART_CR1(USART3) &= ~USART_CR1_TCIE;
	}*/
}


//wysłanie znaków przez USART1
int32_t usart1_write(char *data, uint32_t size) {
    //wpisujemy dane do buffora
    ring_write(&output1_ring, data, size);
    //ustawiamy flagę przewania od wysłania
	//uart1_send_message=rtc_get();
    return uart1_send_message;
}

int32_t usart1_read(char *data, uint32_t size, uint16_t timeout) {
    return ring_read(&input1_ring, data, size, timeout);
}