#include <libopencm3/stm32/flash.h>

#include "flash.h"
//#include "delay.h"
void flash_read(uint32_t start_address, uint16_t num_elements, uint8_t *output_data)
{
	uint16_t iter;
	uint32_t *memory_ptr= (uint32_t*)start_address;

	for(iter=0; iter<num_elements/4; iter++)
	{
		*(uint32_t*)output_data = *(memory_ptr + iter);
		output_data += 4;
	}
}

uint32_t flash_write(uint32_t start_address, uint8_t *input_data, uint16_t num_elements) {
	flash_prefetch_disable();
	uint16_t iter;
	uint32_t current_address = start_address;
	uint32_t page_address = start_address;
	uint32_t flash_status = 0;

	/*check if start_address is in proper range*/
	if((start_address - FLASH_BASE) >= (FLASH_PAGE_SIZE * (FLASH_PAGE_NUM_MAX+1)))
		return 1;

	/*calculate current page address*/
	if(start_address % FLASH_PAGE_SIZE)
		page_address -= (start_address % FLASH_PAGE_SIZE);

	flash_unlock();

	/*Erasing page*/
	//flash_erase_page(page_address); F4XX fix
	//flash_status = flash_get_status_flags();
	//if(flash_status != FLASH_SR_EOP) 
	//	return flash_status;

	/*programming flash memory*/
	for(iter=0; iter<num_elements; iter += 4)
	{
		/*programming word data*/
		flash_program_word(current_address+iter, *((uint32_t*)(input_data + iter)));
		//flash_status = flash_get_status_flags(); F4XX fix
		if(flash_status != FLASH_SR_EOP)
			return flash_status;

		/*verify if correct data is programmed*/
		if(*((uint32_t*)(current_address+iter)) != *((uint32_t*)(input_data + iter)))
			return FLASH_WRONG_DATA_WRITTEN;
	}
	flash_lock();
	flash_prefetch_enable();
	return 0;
}

uint32_t flash_update(uint32_t start_address, uint8_t *input_data, uint16_t num_elements) {
	//uint32_t page_address = start_address;

	if (num_elements > 128) {
		return -1;
	}
	
	if( start_address % FLASH_PAGE_SIZE ) {
		//page_address -= (start_address % FLASH_PAGE_SIZE);
		return -1;
	}
	int ret=-1;
	uint8_t read_data[128];
	flash_read(start_address, 128, read_data);

	for (int i=0; i < num_elements; i++) {
		char rch = read_data[i];
		char ich = input_data[i];
		if (read_data[i] != input_data[i]) {
			read_data[i] = input_data[i];
			ret=i;
		}
	}

	if (ret > -1) {
		return flash_write(start_address, read_data, 128);
	} else {
		return -1;
	}
	
}