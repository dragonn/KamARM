#ifndef CLOCK_H
#define CLOCK_H

extern void clock_init(void);
extern void clock_systick(unsigned int us);
#endif