#ifndef DIR_H
#define DIR_H

#include <libopencm3/stm32/gpio.h>

struct DIR {
    uint16_t pinA;
    uint32_t portsA;

    uint16_t pinB;
    uint32_t portsB;
};

extern volatile struct DIR dirs[];

void dir_init(void);
void dir_forward(volatile struct DIR *dir);
void dir_backward(volatile struct DIR *dir);
void dir_clear(volatile struct DIR *dir);

#endif