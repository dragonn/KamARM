#ifndef AB_H
#define AB_H

void ab_init(void);
void ab_set(uint16_t ssi1, uint16_t ssi2, uint16_t ssi3, uint16_t ssi4);
#endif