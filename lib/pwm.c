#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include "pwm.h"
volatile struct PWM pwms[11];

void pwm_conf(volatile struct PWM *pwm);

void pwm_init(void) {
    pwms[0].ports=GPIOE;
    pwms[0].pin=GPIO9;
    pwms[0].set=&TIM_CCR1(TIM1);
    pwms[0].af=GPIO_AF1;

    pwms[1].set=&TIM_CCR2(TIM1);
    pwms[1].ports=GPIOE;
    pwms[1].pin=GPIO11;
    pwms[1].af=GPIO_AF1;

    pwms[2].set=&TIM_CCR3(TIM1);
    pwms[2].ports=GPIOE;
    pwms[2].pin=GPIO13;
    pwms[2].af=GPIO_AF1;

    pwms[3].set=&TIM_CCR4(TIM1);
    pwms[3].ports=GPIOE;
    pwms[3].pin=GPIO14;
    pwms[3].af=GPIO_AF1;

    pwms[4].set=&TIM_CCR1(TIM9);
    pwms[4].ports=GPIOE;
    pwms[4].pin=GPIO5;
    pwms[4].af=GPIO_AF3;

    pwms[5].set=&TIM_CCR2(TIM9);
    pwms[5].ports=GPIOE;
    pwms[5].pin=GPIO6;
    pwms[5].af=GPIO_AF3;

    pwms[6].set=&TIM_CCR3(TIM2);
    pwms[6].ports=GPIOA;
    pwms[6].pin=GPIO2;
    pwms[6].af=GPIO_AF1;

    pwms[7].set=&TIM_CCR4(TIM2);
    pwms[7].ports=GPIOA;
    pwms[7].pin=GPIO3;
    pwms[7].af=GPIO_AF1;

    pwms[8].set=&TIM_CCR1(TIM2);
    pwms[8].ports=GPIOA;
    pwms[8].pin=GPIO5;
    pwms[8].af=GPIO_AF1;

    pwms[9].set=&TIM_CCR1(TIM12);
    pwms[9].ports=GPIOB;
    pwms[9].pin=GPIO14;
    pwms[9].af=GPIO_AF1;

    pwms[10].set=&TIM_CCR2(TIM12);
    pwms[10].ports=GPIOB;
    pwms[10].pin=GPIO15;
    pwms[10].af=GPIO_AF1;

    for (uint8_t i=0; i< 11; i++) {
        pwm_conf(&pwms[i]);
    }
}

void pwm_conf(volatile struct PWM *pwm) {
    gpio_mode_setup(pwm->ports, GPIO_MODE_AF, GPIO_PUPD_NONE, pwm->pin);
    //gpio_set_output_options(pwm->ports, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, pwm->pin);
    gpio_set_af(pwm->ports, pwm->af, pwm->pin);
}
