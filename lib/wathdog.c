#include <libopencm3/stm32/iwdg.h>

#include "wathdog.h"

void wathdog_init() {
    iwdg_start();
}

void wathdog_set(uint32_t period) {
    iwdg_set_period_ms(period);
}