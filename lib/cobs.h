#ifndef COBS_H
#define COBS_H

#include <stdio.h>

uint16_t cobs_encode(const uint8_t *buffer, uint16_t size, uint8_t *encoded);
uint16_t cobs_decode(const uint8_t *encoded, uint16_t size, uint8_t *decoded);

#endif