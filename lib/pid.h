#ifndef PID_H
#define PID_H

#include "pwm.h"
#include "dir.h"

struct PID {
    float deviation1, deviation2, deviation3;
    float control1, control2;
    float sum_p, sum_i, sum_d;
    float prop, iner, dead;
    float desired_value;
    volatile struct PWM *pwm;
    volatile struct DIR *dir;
    volatile uint32_t *current;
    uint16_t center;
    bool enabled;
};

extern volatile struct PID pid1;
extern volatile struct PID pid2;
extern volatile struct PID pid3;
extern volatile struct PID pid4;

void pid_init(void);
void pid_calc(volatile struct PID *pid);
void pid_angle(int16_t angle1, int16_t angle2);
void pid_save(void);
#endif