#ifndef WATHDOG_H
#define WATHDOG_H

#include <libopencm3/stm32/iwdg.h>

void wathdog_init();
void wathdog_set(uint32_t period);

inline static void wathdog_reset() {
    iwdg_reset();
};

#endif
