/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */






#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>


#include <libopencm3/cm3/nvic.h>
//#include <libopencm3/stm32/flash.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/timer.h>

/*#include "lib/ssi.h"
#include "lib/encoders.h"
#include "lib/clock.h"

#include "lib/pid.h"

#include "lib/ab.h"
#include "lib/uart.h"
#include "lib/wathdog.h"

#include "lib/flash.h"*/

#include "lib/dir.h"
#include "lib/clock.h"
#include "lib/pwm.h"
#include "lib/timers.h"
#include "lib/encoders.h"

void sys_tick_handler(void) {
	/*pwm_calc(&pwm1);
	pwm_calc(&pwm2);
	pwm_calc(&pwm3);
	pwm_calc(&pwm4);
	pwm_calc(&pwm5);
	wathdog_reset();*/
	//gpio_toggle(GPIOF, GPIO10);
}


uint32_t rtccount = 0;
int main(void) {
	clock_init();
	
	//clock_systick((1000/255)*4);
	clock_systick(10000000);

	
	dir_init();
	timers_init();
	pwm_init();
	encoders_init();
	ssi_init();
	*pwms[0].set=1900;
	*pwms[1].set=1024;
	*pwms[2].set=1;
	*pwms[3].set=1999;

	*pwms[4].set=512;
	*pwms[5].set=200;
	*pwms[6].set=700;
	*pwms[7].set=900;
	*pwms[8].set=1500;
	/*clock_init();
	rtc_init();
	pwm_init();
	
	
	encoders_init();

	uint16_t ssi1, ssi2, ssi3, ssi4;
	ssi_read(&ssi1, &ssi2, &ssi3, &ssi4);

	ab_init();
	ab_set(ssi1, ssi2, ssi3, ssi4);


	
	//to-test
	pwm_init();
	//to-test
	
	//to-test
	pid_init();
	pid_read();
	//to-do, not working
	
	pwm1.set=0;
	pwm2.set=0;
	pwm3.set=0;
	pwm4.set=0;
	pwm5.set=128;

	dir_clear(&dir1);
	dir_clear(&dir2);
	dir_clear(&dir3);
	dir_clear(&dir4);
	dir_clear(&dir5);

	uart1_init();
	//usart1_write("start\n\r", 7);

	wathdog_init();
	wathdog_set(50);
	clock_systick((1000/255)*4);*/
	rcc_periph_clock_enable(RCC_GPIOF);
	gpio_mode_setup(GPIOF, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO10);
	while(true) {
		
		/*if (rtc_get() - uart1_last_message > 1000) {
			pid1.enabled=false;
			pid2.enabled=false;
			pid3.enabled=false;
			pid4.enabled=false;
			pwm5.set=0;
			dir_clear(&dir5);
		}

		if (uart1_send_message !=0 && rtc_get() - uart1_send_message > 1) {
			uart1_send_message=0;
			USART_CR1(USART3) |= USART_CR1_TXEIE;
		}

		pid_calc(&pid1);
		pid_calc(&pid2);
		pid_calc(&pid3);
		pid_calc(&pid4);
		
		wathdog_reset();*/

		//gpio_toggle(GPIOF, GPIO9);	/* LED on/off */
		/*for (int i = 0; i < 1000000; i++) {	
			__asm__("nop");
		}*/
        //gpio_toggle(GPIOF, GPIO9);
        /*for (int i = 0; i < 300000; i++) {
                        __asm__("nop");
        }*/
		
		gpio_toggle(GPIOF, GPIO10);
	}
	return 0;
}
