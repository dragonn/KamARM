#ifndef PINS_H_
#define PINS_H_
#include "stm32f4xx.h"
#include "bitband.h"

//==================H=BRIGDE=CONTROL============================
#define ENG1A_PORT		GPIOF
#define ENG1A_PIN		14U
#define ENG1A			ENG1A_PORT->ODR

#define ENG1B_PORT		GPIOF
#define ENG1B_PIN		15U
#define ENG1B			ENG1B_PORT->ODR
//===============================================================
#define ENG2A_PORT		GPIOG
#define ENG2A_PIN		0U
#define ENG2A			ENG2A_PORT->ODR

#define ENG2B_PORT		GPIOG
#define ENG2B_PIN		1U
#define ENG2B			ENG2B_PORT->ODR
//===============================================================
#define ENG3A_PORT		GPIOG
#define ENG3A_PIN		2U
#define ENG3A			ENG3A_PORT->ODR

#define ENG3B_PORT		GPIOG
#define ENG3B_PIN		3U
#define ENG3B			ENG3B_PORT->ODR
//===============================================================
#define ENG4A_PORT		GPIOG
#define ENG4A_PIN		4U
#define ENG4A			ENG4A_PORT->ODR

#define ENG4B_PORT		GPIOG
#define ENG4B_PIN		5U
#define ENG4B			ENG4B_PORT->ODR
//===============================================================
#define ENG5A_PORT		GPIOG
#define ENG5A_PIN		6U
#define ENG5A			ENG5A_PORT->ODR

#define ENG5B_PORT		GPIOG
#define ENG5B_PIN		7U
#define ENG5B			ENG5B_PORT->ODR
//===============================================================
#define ENG6A_PORT		GPIOG
#define ENG6A_PIN		8U
#define ENG6A			ENG6A_PORT->ODR

#define ENG6B_PORT		GPIOG
#define ENG6B_PIN		9U
#define ENG6B			ENG6B_PORT->ODR
//===============================================================
#define ENG7A_PORT		GPIOG
#define ENG7A_PIN		10U
#define ENG7A			ENG7A_PORT->ODR

#define ENG7B_PORT		GPIOG
#define ENG7B_PIN		11U
#define ENG7B			ENG7B_PORT->ODR
//===============================================================
#define ENG8A_PORT		GPIOG
#define ENG8A_PIN		12U
#define ENG8A			ENG8A_PORT->ODR

#define ENG8B_PORT		GPIOG
#define ENG8B_PIN		13U
#define ENG8B			ENG8B_PORT->ODR
//===============================================================
#define ENG9A_PORT		GPIOG
#define ENG9A_PIN		14U
#define ENG9A			ENG9A_PORT->ODR

#define ENG9B_PORT		GPIOG
#define ENG9B_PIN		15U
#define ENG9B			ENG9B_PORT->ODR


//==============A===========================
#define ENG1A_SET BB(&ENG1A, ENG1A_PIN) = 1;
#define ENG1A_CLEAR BB(&ENG1A, ENG1A_PIN) = 0;

#define ENG2A_SET BB(&ENG2A, ENG2A_PIN) = 1;
#define ENG2A_CLEAR BB(&ENG2A, ENG2A_PIN) = 0;

#define ENG3A_SET BB(&ENG3A, ENG3A_PIN) = 1;
#define ENG3A_CLEAR BB(&ENG3A, ENG3A_PIN) = 0;

#define ENG4A_SET BB(&ENG4A, ENG4A_PIN) = 1;
#define ENG4A_CLEAR BB(&ENG4A, ENG4A_PIN) = 0;

#define ENG5A_SET BB(&ENG5A, ENG5A_PIN) = 1;
#define ENG5A_CLEAR BB(&ENG5A, ENG5A_PIN) = 0;

#define ENG6A_SET BB(&ENG6A, ENG6A_PIN) = 1;
#define ENG6A_CLEAR BB(&ENG6A, ENG6A_PIN) = 0;

#define ENG7A_SET BB(&ENG7A, ENG7A_PIN) = 1;
#define ENG7A_CLEAR BB(&ENG7A, ENG7A_PIN) = 0;

#define ENG8A_SET BB(&ENG8A, ENG8A_PIN) = 1;
#define ENG8A_CLEAR BB(&ENG8A, ENG8A_PIN) = 0;

#define ENG9A_SET BB(&ENG9A, ENG9A_PIN) = 1;
#define ENG9A_CLEAR BB(&ENG9A, ENG9A_PIN) = 0;
//=============B=============================

#define ENG1B_SET BB(&ENG1B, ENG1B_PIN) = 1;
#define ENG1B_CLEAR BB(&ENG1B, ENG1B_PIN) = 0;

#define ENG2B_SET BB(&ENG2B, ENG2B_PIN) = 1;
#define ENG2B_CLEAR BB(&ENG2B, ENG2B_PIN) = 0;

#define ENG3B_SET BB(&ENG3B, ENG3B_PIN) = 1;
#define ENG3B_CLEAR BB(&ENG3B, ENG3B_PIN) = 0;

#define ENG4B_SET BB(&ENG4B, ENG4B_PIN) = 1;
#define ENG4B_CLEAR BB(&ENG4B, ENG4B_PIN) = 0;

#define ENG5B_SET BB(&ENG5B, ENG5B_PIN) = 1;
#define ENG5B_CLEAR BB(&ENG5B, ENG5B_PIN) = 0;

#define ENG6B_SET BB(&ENG6B, ENG6B_PIN) = 1;
#define ENG6B_CLEAR BB(&ENG6B, ENG6B_PIN) = 0;

#define ENG7B_SET BB(&ENG7B, ENG7B_PIN) = 1;
#define ENG7B_CLEAR BB(&ENG7B, ENG7B_PIN) = 0;

#define ENG8B_SET BB(&ENG8B, ENG8B_PIN) = 1;
#define ENG8B_CLEAR BB(&ENG8B, ENG8B_PIN) = 0;

#define ENG9B_SET BB(&ENG9B, ENG9B_PIN) = 1;
#define ENG9B_CLEAR BB(&ENG9B, ENG9B_PIN) = 0;
//===========================================

//==============ADC==PINOUT==================
#define ADC1_PORT		GPIOF
#define ADC1_PIN		3U
#define ADC1_CH			9U 				//Number of Regular Channel for ADC3_IN

#define ADC2_PORT		GPIOF
#define ADC2_PIN		4U
#define ADC2_CH			14U				//Number of Regular Channel for ADC3_IN

#define ADC3_PORT		GPIOF
#define ADC3_PIN		5U
#define ADC3_CH			15U				//Number of Regular Channel for ADC3_IN

#define ADC4_PORT		GPIOF
#define ADC4_PIN		6U
#define ADC4_CH			4U				//Number of Regular Channel for ADC3_IN

#define ADC5_PORT		GPIOF
#define ADC5_PIN		7
#define ADC5_CH			5				//Number of Regular Channel for ADC3_IN

#define ADC6_PORT		GPIOF
#define ADC6_PIN		8
#define ADC6_CH			6				//Number of Regular Channel for ADC3_IN

#define ADC7_PORT		GPIOC
#define ADC7_PIN		0
#define ADC7_CH			10				//Number of Regular Channel for ADC3_IN

#define ADC8_PORT		GPIOC
#define ADC8_PIN		1
#define ADC8_CH			11				//Number of Regular Channel for ADC3_IN

#define ADC9_PORT		GPIOC
#define ADC9_PIN		2
#define ADC9_CH			12				//Number of Regular Channel for ADC3_IN

#define ADC10_PORT		GPIOC
#define ADC10_PIN		3
#define ADC10_CH		13				//Number of Regular Channel for ADC3_IN
//===========================================

//==============PWM==PINOUT==================
#define PWM1_PORT		GPIOE
#define PWM1_PIN		9
#define PWM1			TIM1->CCR1

#define PWM2_PORT		GPIOE
#define PWM2_PIN		11
#define PWM2			TIM1->CCR2

#define PWM3_PORT		GPIOE
#define PWM3_PIN		13
#define PWM3			TIM1->CCR3

#define PWM4_PORT		GPIOE
#define PWM4_PIN		14
#define PWM4			TIM1->CCR4

#define PWM5_PORT		GPIOE
#define PWM5_PIN		5
#define PWM5			TIM9->CCR1

#define PWM6_PORT		GPIOE
#define PWM6_PIN		6
#define PWM6			TIM9->CCR2

#define PWM7_PORT		GPIOA
#define PWM7_PIN		2
#define PWM7			TIM2->CCR3

#define PWM8_PORT		GPIOA
#define PWM8_PIN		3
#define PWM8			TIM2->CCR4

#define PWM9_PORT		GPIOA
#define PWM9_PIN		5
#define PWM9			TIM2->CCR1

#define PWM10_PORT		GPIOB
#define PWM10_PIN		14
#define PWM10			TIM12->CCR1

#define PWM11_PORT		GPIOB
#define PWM11_PIN		15
#define PWM11			TIM12->CCR2
//===========================================

//==============RS485======UART1=============
#define RS485_TX_PORT		GPIOB
#define RS485_TX_PIN		6

#define RS485_RX_PORT		GPIOB
#define RS485_RX_PIN		7

#define RS485_CONTROL_PORT		GPIOB
#define RS485_CONTROL_PIN		8
#define RS485_CONTROL			RS485_CONTROL_PORT->ODR

#define RS485_SET BB(&RS485_CONTROL, RS485_CONTROL_PIN) = 1;
#define RS485_CLEAR BB(&RS485_CONTROL, RS485_CONTROL_PIN) = 0;
//===========================================

//==============SSI=INTERFACE================
#define SSI1_PORT		GPIOD
#define SSI1_PIN		0

#define SSI1_IDR		SSI1_PORT->IDR
#define SSI1_MSK		(1U << SSI1_PIN)
#define SSI1			(SSI1_IDR & SSI1_MSK)


#define SSI2_PORT		GPIOD
#define SSI2_PIN		1

#define SSI2_IDR		SSI2_PORT->IDR
#define SSI2_MSK		(1U << SSI2_PIN)
#define SSI2			(SSI2_IDR & SSI2_MSK)


#define SSI3_PORT		GPIOD
#define SSI3_PIN		2

#define SSI3_IDR		SSI3_PORT->IDR
#define SSI3_MSK		(1U << SSI3_PIN)
#define SSI3			(SSI3_IDR & SSI3_MSK)


#define SSI4_PORT		GPIOD
#define SSI4_PIN		3

#define SSI4_IDR		SSI4_PORT->IDR
#define SSI4_MSK		(1U << SSI4_PIN)
#define SSI4			(SSI4_IDR & SSI4_MSK)


#define SSI5_PORT		GPIOD
#define SSI5_PIN		4

#define SSI5_IDR		SSI5_PORT->IDR
#define SSI5_MSK		(1U << SSI5_PIN)
#define SSI5			(SSI5_IDR & SSI5_MSK)


#define SSI6_PORT		GPIOD
#define SSI6_PIN		5

#define SSI6_IDR		SSI6_PORT->IDR
#define SSI6_MSK		(1U << SSI6_PIN)
#define SSI6			(SSI6_IDR & SSI6_MSK)


#define SSI7_PORT		GPIOD
#define SSI7_PIN		6

#define SSI7_IDR		SSI7_PORT->IDR
#define SSI7_MSK		(1U << SSI7_PIN)
#define SSI7			(SSI7_IDR & SSI7_MSK)


#define SSI8_PORT		GPIOD
#define SSI8_PIN		7

#define SSI8_IDR		SSI8_PORT->IDR
#define SSI8_MSK		(1U << SSI8_PIN)
#define SSI8			(SSI8_IDR & SSI8_MSK)


#define SSI9_PORT		GPIOD
#define SSI9_PIN		8

#define SSI9_IDR		SSI9_PORT->IDR
#define SSI9_MSK		(1U << SSI9_PIN)
#define SSI9			(SSI9_IDR & SSI9_MSK)

#define SSI_CLOCK_PORT		GPIOD
#define SSI_CLOCK_PIN		9
#define SSI_CLOCK_ODR		SSI_CLOCK_PORT->ODR

#define SSI_CLOCK_SET		BB(&SSI_CLOCK_ODR, SSI_CLOCK_PIN) = 1;
#define SSI_CLOCK_RESET		BB(&SSI_CLOCK_ODR, SSI_CLOCK_PIN) = 0;
//===========================================

//=====HARDWARE===ENCODER===INTERFACE============================
//=====TIM5====================================================
#define ENC1A_PORT		GPIOA
#define ENC1A_PIN		0U

#define ENC1B_PORT		GPIOA
#define ENC1B_PIN		1U

#define ENC1			TIM5->CNT
//=====TIM3======================================================
#define ENC2A_PORT		GPIOA
#define ENC2A_PIN		6U

#define ENC2B_PORT		GPIOA
#define ENC2B_PIN		7U

#define ENC2			TIM3->CNT
//=====TIM8======================================================
#define ENC3A_PORT		GPIOC
#define ENC3A_PIN		6U

#define ENC3B_PORT		GPIOC
#define ENC3B_PIN		7U

#define ENC3			TIM8->CNT
//=====TIM4======================================================
#define ENC4A_PORT		GPIOD
#define ENC4A_PIN		12U

#define ENC4B_PORT		GPIOD
#define ENC4B_PIN		13U

#define ENC4			TIM4->CNT
//===============================================================

//======OTHER==PINS=============================================
#define TRACE_PORT		GPIOB
#define TRACE_PIN		3U

#define LED1_PORT		GPIOF
#define LED1_PIN		9U
#define LED1			BB(&LED1_PORT->ODR, LED1_PIN)

#define LED2_PORT		GPIOF
#define LED2_PIN		10U
#define LED2			BB(&LED2_PORT->ODR, LED2_PIN)
//===============================================================

void RccClockBusInit(void);
void EngineInitGpio(void);
void AdcInitGpio(void);
void PwmInitGpio(void);
void RS485InitGpio(void);
void SsiInitGpio(void);
void EncInitGpio(void);
void OtherInitGpio(void);

void PwmTimerInit(void);
void EncTimerInit(void);
